<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Illuminate\Support\Facades\Cache;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Cache::remember('blogs', 5, function () {
            return Blog::all();
        });
        return view('Blog', compact('blogs'));
    }
}
