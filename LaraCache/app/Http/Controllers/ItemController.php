<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Illuminate\Support\Facades\Cache;

class ItemController extends Controller
{
    public function indexCache()
    {
        $items = Cache::remember('items', 50, function () {
            return Item::select('item')->get();
        });
        return view('itemCache', compact('items'));
    }

    public function index()
    {
        $items = Item::select('item')->get();
        return view('item', compact('items'));
    }

    public function store()
    {
        $p = 'Oke saya sheva, 
        mungkin hal yang saya pahami dari redis dan fitur cache ini 
        adalah kegunaannya kegunaan fitur cache adalah untuk mengurangi request ke database,
        apabila ada yg melakukan request sekali maka request tsb akan tersimpan pada keys di redis,
        fitur cache ini sangat bermanfaat apabila banyak user yang sedang mengakses web yang kita buat,
        karena sering ditemukan web yang memerlukan beberapa menit untuk melakukan load data dari database,
        nah fitur dari cache ini meminimalkan request tersebut agar user tidak melakukan requst yang sama ke database secara
        berulang ulang';

        $p_arr = explode(" ", $p);

        $arr = array_filter($p_arr);
        foreach ($arr as $key => $val) {
            Item::create([
                'item' => $val
            ]);
        }
        return $arr;
    }
}
