<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Item</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>

<body>
    <div class="container">
        <div class="mt-3">
            <h1>Paragraf Tanpa Menggunakan Fitur Cache</h1>
            <p>
            @foreach ($items as $item)
                {{ $item->item.' ' }}
            @endforeach
            </p>
        </div>
    </div>
</body>

</html>
