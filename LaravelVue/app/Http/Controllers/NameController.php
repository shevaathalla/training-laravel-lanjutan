<?php

namespace App\Http\Controllers;

use App\Http\Resources\NameResource;
use App\Name;
use Illuminate\Http\Request;

class NameController extends Controller
{
    public function index()
    {
        //
        $names = Name::orderBy('created_at', 'DESC')->get();
        $all = (NameResource::collection($names));
        return $all;
    }

    public function store(Request $request)
    {
        $name = Name::create([
            'text' => $request->text,
            'role' => 0
        ]);

        return $name;
    }

    public function destroy($id)
    {
        $name = Name::destroy($id);
        return 'success';
    }

    public function changeAdmin($id)
    {
        $name = Name::find($id);
        $update = 0;

        if ($name->role == 1) {
            $update = 0;
        } else {
            $update = 1;
        }

        $name->update([
            'role' => $update
        ]);
        return 'updated';
    }
}
