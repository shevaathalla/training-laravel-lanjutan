<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://use.fontawesome.com/ef0edd48d2.js"></script>
    <title>LaraName</title>
</head>

<body>
    <div id="app">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control form-control-lg mb-2" type="text" placeholder="Inputan Nama" v-model="newName" @keyup.enter='addName'>
                </div>
                <div class="col">
                    <div class="float-right">
                        <button class="btn btn-primary btn-lg" v-on:click="addName">Submit</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(name, index) in names" v-bind:class="{'table-success' : name.role}">
                                <th scope="row">@{{ index+1 }}</th>
                                <td>@{{ name.text }}</td>
                                <td>
                                    <button class="btn btn-danger" v-on:click='removeName(index,name)'><i class="fa fa-trash">&nbsp;&nbsp;Delete</i></button>
                                    <button class="btn btn-success" v-on:click='makeAdmin(name)'>
                                        <div v-if='name.role'>
                                            <i class="fa fa-unlock">&nbsp;&nbsp; Release Admin</i>
                                        </div>
                                        <div v-else>
                                            <i class="fa fa-lock">&nbsp;&nbsp; Make Admin</i>
                                        </div>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src={{ asset('js/vue.js') }}></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        const vo = new Vue({
            el: '#app',
            data: {
                test: "test",
                newName: "",
                names: [],
            },
            methods: {
                addName() {
                    let name = this.newName.trim();
                    if (name) {                            
                            this.$http.post('/api/index', {text: name}).then(response => {
                            this.names.unshift({text : name, role:0});
                            this.newName = '';
                        });
                    }                    
                },
                removeName(index, name){                                 
                    this.$http.post('/api/index/delete/'+name.id).then(response => {
                        this.names.splice(index,1);
                    });                    
                },
                makeAdmin(name){
                    this.$http.post('/api/index/change-admin/'+name.id).then(response => {
                        name.role = !name.role;
                    });                                        
                }
            },
            mounted() {                
                this.$http.get('/api/index')
                .then(
                    response => 
                    {
                        let result = response.body.data;
                        this.names = result;
                    });
            },
        });
    </script>
</body>
</html>
