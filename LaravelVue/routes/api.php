<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('index', 'NameController@index');
Route::post('index', 'NameController@store');
Route::post('index/change-admin/{id}', 'NameController@changeAdmin');
Route::post('index/delete/{id}', 'NameController@destroy');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
