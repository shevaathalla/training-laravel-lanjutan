<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeminjamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjamen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('tgl_peminjaman');
            $table->dateTime('tgl_batas_akhir');
            $table->dateTime('tgl_pengembalian')->nullable();
            $table->string('nim');
            $table->foreign('nim')->references('nim')->on('users');
            $table->unsignedBigInteger('id_buku');
            $table->foreign('id_buku')->references('id')->on('books');
            $table->boolean('ontime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjamen');
    }
}
