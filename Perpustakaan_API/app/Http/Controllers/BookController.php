<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Peminjaman;
use Carbon\Carbon;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_buku)
    {
        $all = Book::all();
        $buku = $all->find($id_buku);
        return $buku;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_buku' => ['required', 'min:4', 'max:4'],
            'judul' => ['required']
        ]);

        $add_book = Book::create([
            'kode_buku' => request('kode_buku'),
            'judul' => request('judul'),
        ]);

        return $add_book;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Book::withTrashed()->where('id', $id);
        $buku->forceDelete();
    }

    public function pinjam($id)
    {
        $buku = Book::find($id);
        $pinjam = Peminjaman::create([
            'tgl_peminjaman' => Carbon::now(),
            'tgl_batas_akhir' => Carbon::now()->addWeek(),
            'nim' => auth()->user()->nim,
            'id_buku' => $buku->id
        ]);
        $buku->delete();
        return $pinjam;
    }

    public function kembali($id)
    {
        $now = Carbon::now();
        $buku = Book::withTrashed()->where('id', $id);
        $pinjam = Peminjaman::where('id_buku', $id)->first();
        // dd($pinjam);
        $pinjam->tgl_pengembalian = $now;
        if ($now->lte(Carbon::parse($pinjam->tgl_batas_akhir))) {
            $pinjam->ontime = true;
        } else {
            $pinjam->ontime = false;
        }
        $pinjam->save();
        // dd($pinjam);
        $buku->restore();
        return $pinjam;
    }

    public function dipinjam()
    {
        $buku = Book::onlyTrashed()->get();
        return $buku;
    }

    public function all()
    {
        $buku = Book::withTrashed()->get();
        return $buku;
    }
}
