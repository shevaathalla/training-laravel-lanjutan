<?php

use App\Http\Middleware\AdminMiddleware;

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});
Route::prefix('book')->group(function () {
    Route::post('add-new-book', 'BookController@store')->middleware('auth:api', 'admin');
    Route::get('{id}', 'BookController@index');
    Route::delete('{id}', 'BookController@destroy')->middleware('auth:api', 'admin');
    Route::post('{id}/pinjam', 'BookController@pinjam')->middleware('auth:api', 'admin');
    Route::post('{id}/kembali', 'BookController@kembali')->middleware('auth:api', 'admin');
});
Route::get('dipinjam', 'BookController@dipinjam');
Route::get('all', 'BookController@all');


Route::get('user', 'UserController');
