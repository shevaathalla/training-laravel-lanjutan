<?php

namespace App\Listeners;

use App\Events\TodoCreatedEvent;
use App\Mail\TodoCreatedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotification implements ShouldQueue
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  TodoCreatedEvent  $event
   * @return void
   */
  public function handle(TodoCreatedEvent $event)
  {
    Mail::to($event->user)->send(new TodoCreatedMail($event->user));
  }
}
