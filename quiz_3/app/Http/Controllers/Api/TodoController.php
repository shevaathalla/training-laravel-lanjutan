<?php

namespace App\Http\Controllers\Api;

use App\Events\TodoCreatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use App\Http\Resources\TodoResource;
use Illuminate\Http\Request;
use App\Models\Todo;


class TodoController extends Controller
{
  public function index()
  {

    $todos = Todo::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();

    return TodoResource::collection($todos);
  }

  public function store(TodoRequest $request)
  {
    // dd(auth()->user());
    $todo = auth()->user()->todos()->create([
      'text' => $request->text,
      'done' => 0
    ]);

    event(new TodoCreatedEvent($todo));

    return new TodoResource($todo);
  }

  public function delete($id)
  {
    $todo = Todo::find($id);
    if (auth()->user()->id != $todo->user_id) {
      return response()->json(['error' => 'Not Allowed'], 403);
    } else {
      Todo::destroy($id);
      return 'success';
    }
  }

  public function changeDoneStatus($id)
  {
    $todo = Todo::find($id);
    if (auth()->user()->id != $todo->user_id) {
      return response()->json(['error' => 'Not Allowed'], 403);
    } else {
      if ($todo->done == 1) {
        $update = 0;
      } else {
        $update = 1;
      }
      $todo->update([
        'done' => $update
      ]);
    }

    return new TodoResource($todo);
  }
}
