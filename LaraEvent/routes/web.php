<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/blog', 'BlogController@index');
Route::get('/create-blog', 'BlogController@create')->middleware('auth');
Route::post('/', 'BlogController@store')->middleware('auth');
Route::get('/review/blog/{id}', 'BlogController@show')->name('review')->middleware('auth');
Route::put('/review/blog/{id}', 'BlogController@update')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
