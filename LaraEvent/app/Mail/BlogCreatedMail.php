<?php

namespace App\Mail;

use App\Blog;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BlogCreatedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Blog $blog)
    {
        $this->user = $user;
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('editor@example.com')
            ->view('blog.send_mail_blog_created')
            ->with([
                'name' => $this->user->name,
                'judul' => $this->blog->judul,
            ]);
    }
}
