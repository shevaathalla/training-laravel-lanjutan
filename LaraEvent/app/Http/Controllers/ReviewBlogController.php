<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class ReviewBlogController extends Controller
{
    public function show($id)
    {
        $blog = Blog::find($id)->first();

        return view('blog.review_blog', compact('blog'));
    }
}
