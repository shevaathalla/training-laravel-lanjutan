<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Events\BlogAcceptedEvent;
use App\Events\BlogCreatedEvent;
use App\Http\Requests\CreateBlogRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::where('status', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create_blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlogRequest $request)
    {
        $data = [];
        $blog = Blog::create([
            'judul' => request('judul'),
            'konten' => request('konten'),
            'user_id' => Auth::id(),
        ]);
        $user = Auth::user();
        $data['blog'] = $blog;

        event(new BlogCreatedEvent($user, $blog));
        return redirect('/')->with('uploaded', 'Blog uploaded wait for review');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::where('id', $id)->first();

        return view('blog.review_blog', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $blog = Blog::find($id);

        $blog->update([
            'judul' => request('judul'),
            'konten' => request('konten'),
            'status' => 1
        ]);

        $user = Auth::user();
        event(new BlogAcceptedEvent($user, $blog));
        return redirect('/')->with('accepted', 'Blog Accepted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
