<?php

namespace App\Listeners;

use App\Events\BlogAcceptedEvent;
use App\Mail\BlogAcceptedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class BlogAcceptedEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogAcceptedEvent  $event
     * @return void
     */
    public function handle(BlogAcceptedEvent $event)
    {
        Mail::to($event->user)->send(new BlogAcceptedMail($event->user, $event->blog));
    }
}
