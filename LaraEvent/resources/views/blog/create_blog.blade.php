@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="/" method="POST">
            @csrf
            <div class="form-group">
                <h3><span class="label label-default">Judul</span></h3>
                <input type="text" class="form-control" id="judul" name="judul" placeholder="Tulis Judul dari Blog anda disini !">
            </div>
            <div class="form-group">
                <h3><span class="label label-default">Konten</span></h3>
                <textarea class="form-control" name="konten" id="konten" cols="30" rows="10" placeholder="Tulis Konten dari blog anda disini !"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="Submit" class="btn btn-success">
            </div>
        </form>        
    </div>
@endsection