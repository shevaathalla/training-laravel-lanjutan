@extends('layouts.app')
@section('content')
<div class="container">
    @foreach ($blogs as $blog)
    <div class="card mb-2">
        <div class="card-header">
            ID : {{ $blog->id }}
        </div>
        <div class="card-body">
            <h3 class="text-bold">{{ $blog->judul }}</h3>
            <p class="card-text">{{ $blog->konten }}</p>            
        </div>
    </div>
    @endforeach    
</div>
@endsection
