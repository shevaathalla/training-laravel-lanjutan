<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog Created Email Notification</title>
</head>
<body>
    <p>Selamat Saudara/i {{ $name }} , blog Anda yang berjudul {{ $judul }}, akan kami review. Kami akan mengirimkan email kepada Anda jika blog Anda sudah berhasil dipublish</p>
</body>
</html>